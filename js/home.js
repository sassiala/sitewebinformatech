window.addEventListener("scroll", function () {
    const header = document.querySelector(".header");
    if (window.scrollY > 0) {
        header.classList.add("header-scroll");
    } else {
        header.classList.remove("header-scroll");
    }

    const navbarLinks = document.querySelectorAll(".navbar_menu a");
    const sections = document.querySelectorAll(".section");
    const scrollPosition = window.scrollY;
    sections.forEach(section => {
        const boundingRect = section.getBoundingClientRect();
        const offsetTop = boundingRect.top + scrollPosition;
        const sectionHeight = section.clientHeight;
        const sectionId = section.getAttribute("id");

        if (scrollPosition >= offsetTop - 250 && scrollPosition < offsetTop + sectionHeight - 50) {

            navbarLinks.forEach(link => link.classList.remove("active"));
            let a = document.querySelector(`.navbar_menu a[data-target="${sectionId}"]`)
            if (a) a.classList.add("active");

        }
    });
});