document.addEventListener("click", e => {
    const isDropDown = e.target.matches("[data-dropdown-button]")
    if(!isDropDown && e.target.closest('[data-dropdown]') != null) return
    let currDropDown 
    if(isDropDown)
    {
        currDropDown = e.target.closest('[data-dropdown]')
        currDropDown.classList.toggle('activeDropDown')
    }
    document.querySelectorAll("[data-dropdown].activeDropDown").forEach(dropDown => {
        if(dropDown === currDropDown) return
        dropDown.classList.remove('activeDropDown')
    })
})