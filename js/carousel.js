const CustomWrapper = document.querySelector(".CustomWrapper");
const CustomCarousel = document.querySelector(".CustomCarousel");
const firstCustomCarouselCardWidth = CustomCarousel.querySelector(".CustomCarouselCard").offsetWidth;
const arrowBtns = document.querySelectorAll(".CustomWrapper i");
const CustomCarouselChildrens = [...CustomCarousel.children];

let isDragging = false, isAutoPlay = true, startX, startScrollLeft, timeoutId;

// Get the number of CustomCarouselCards that can fit in the CustomCarousel at once
let CustomCarouselCardPerView = Math.round(CustomCarousel.offsetWidth / firstCustomCarouselCardWidth);

// Insert copies of the last few CustomCarouselCards to beginning of CustomCarousel for infinite scrolling
CustomCarouselChildrens.slice(-CustomCarouselCardPerView).reverse().forEach(CustomCarouselCard => {
    CustomCarousel.insertAdjacentHTML("afterbegin", CustomCarouselCard.outerHTML);
});

// Insert copies of the first few CustomCarouselCards to end of CustomCarousel for infinite scrolling
CustomCarouselChildrens.slice(0, CustomCarouselCardPerView).forEach(CustomCarouselCard => {
    CustomCarousel.insertAdjacentHTML("beforeend", CustomCarouselCard.outerHTML);
});

// Scroll the CustomCarousel at appropriate postition to hide first few duplicate CustomCarouselCards on Firefox
CustomCarousel.classList.add("no-transition");
CustomCarousel.scrollLeft = CustomCarousel.offsetWidth;
CustomCarousel.classList.remove("no-transition");

// Add event listeners for the arrow buttons to scroll the CustomCarousel left and right
arrowBtns.forEach(btn => {
    btn.addEventListener("click", () => {
        CustomCarousel.scrollLeft += btn.id == "left" ? -firstCustomCarouselCardWidth : firstCustomCarouselCardWidth;
    });
});

const dragStart = (e) => {
    isDragging = true;
    CustomCarousel.classList.add("dragging");
    // Records the initial cursor and scroll position of the CustomCarousel
    startX = e.pageX;
    startScrollLeft = CustomCarousel.scrollLeft;
}

const dragging = (e) => {
    if(!isDragging) return; // if isDragging is false return from here
    // Updates the scroll position of the CustomCarousel based on the cursor movement
    CustomCarousel.scrollLeft = startScrollLeft - (e.pageX - startX);
}

const dragStop = () => {
    isDragging = false;
    CustomCarousel.classList.remove("dragging");
}

const infiniteScroll = () => {
    // If the CustomCarousel is at the beginning, scroll to the end
    if(CustomCarousel.scrollLeft === 0) {
        CustomCarousel.classList.add("no-transition");
        CustomCarousel.scrollLeft = CustomCarousel.scrollWidth - (2 * CustomCarousel.offsetWidth);
        CustomCarousel.classList.remove("no-transition");
    }
    // If the CustomCarousel is at the end, scroll to the beginning
    else if(Math.ceil(CustomCarousel.scrollLeft) === CustomCarousel.scrollWidth - CustomCarousel.offsetWidth) {
        CustomCarousel.classList.add("no-transition");
        CustomCarousel.scrollLeft = CustomCarousel.offsetWidth;
        CustomCarousel.classList.remove("no-transition");
    }

    // Clear existing timeout & start autoplay if mouse is not hovering over CustomCarousel
    clearTimeout(timeoutId);
    if(!CustomWrapper.matches(":hover")) autoPlay();
}

const autoPlay = () => {
    if(window.innerWidth < 800 || !isAutoPlay) return; // Return if window is smaller than 800 or isAutoPlay is false
    // Autoplay the CustomCarousel after every 2500 ms
    timeoutId = setTimeout(() => CustomCarousel.scrollLeft += firstCustomCarouselCardWidth, 1500);
}
autoPlay();

CustomCarousel.addEventListener("mousedown", dragStart);
CustomCarousel.addEventListener("mousemove", dragging);
document.addEventListener("mouseup", dragStop);
CustomCarousel.addEventListener("scroll", infiniteScroll);
CustomWrapper.addEventListener("mouseenter", () => clearTimeout(timeoutId));
CustomWrapper.addEventListener("mouseleave", autoPlay);